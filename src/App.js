import { useState, useRef, useEffect } from 'react'
import './App.css';
import { v4 as uuidv4 } from "uuid";
import TodoList from './TodoList';

const localStorageKey = "this_is_local_storage_key"

function App() {
  const [todos, setTodos] = useState([]);
  const todoNameRef = useRef();

  useEffect(() => {
    const storedTodos = JSON.parse(localStorage.getItem(localStorageKey))
    if (storedTodos) setTodos(storedTodos)
  }, [])

  useEffect(() => {
    localStorage.setItem(localStorageKey, JSON.stringify(todos))
  }, [todos])

  function toggleTodo(id) {
    const newTodos = [...todos]
    const todo = newTodos.find((todo) => todo.id === id);
    todo.complete = !todo.complete;
    setTodos(newTodos);
  }

  function clearTodos() {
    const newTodos = [...todos];
    const todo = newTodos.filter((todo) => todo.complete === false)
    setTodos(todo);
  }

  function handleTodo(e) {
    const name = todoNameRef.current.value;
    console.log(name)
    if (name === "") return;
    setTodos((prevTodos) => {
      return [...prevTodos, { id: uuidv4(), name: name, complete: false }];
    })
    todoNameRef.current.value = null;
  }


  return (
    <div className="App">
      {/* {todos.map((t) => { return <h2> {t.name} </h2> })} */}
      <TodoList todos={todos} toggleTodo={toggleTodo} ></TodoList>
      <input ref={todoNameRef} type="text"></input>
      <button onClick={handleTodo}>Add Todo</button>
      <button onClick={clearTodos}>Clear Completed Todos</button>

    </div>
  );
}

export default App;
